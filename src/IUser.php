<?php
/**
 * Created by PhpStorm.
 * User: jefin
 * Date: 15/09/2016
 * Time: 10:33
 */

namespace ModernWays\Identity;

interface IUser
{
    public function getIsAuthenticated();
    public function setIsAuthenticated($isAuthenticated);
    public function getRoleId();
    public function setRoleId($roleId);
    public function getName();
    public function setName($name);
    public function getPersonId();
    public function setPersonId($personId);
    public function setSalt($salt);
    public function getSalt();
    public function setHashedPassword($hashedPassword);
    public function getHashedPassword();
}

