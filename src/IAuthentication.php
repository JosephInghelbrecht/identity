<?php
/**
 * Created by ModernWays
 * Template: Jef Inghelbrecht
 * Date: 14/07/2015
 * Time: 22:41
 * output from the database
 * input is user input
 */
namespace ModernWays\Identity;

interface IAuthentication
{
    function setSession($value);

    function getNoticeboard();

    function setNoticeboard($noticeboard);

    function setLoginAttempts($value);

    function hashPassword($passwordInput);

    function getUserName();

    function getUserId();

    function login($userNameInput, $passwordInput);

    function isLoggedIn();
}



