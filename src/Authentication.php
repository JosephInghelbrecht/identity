<?php
/**
 * Created by ModernWays
 * Template: Jef Inghelbrecht
 * Date: 14/07/2015
 * Time: 22:41
 * output is from the database
 * input is user input
 */
namespace ModernWays\Identity;

class Authentication implements IAuthentication
{
    private $session;
    private $loginAttempts;
    private $noticeboard;
    private $isVerified;
    private $user;

    public function setSession($value)
    {
        $this->session = $value;
    }

    /**
     * @return mixed
     */
    public function getNoticeboard()
    {
        return $this->noticeboard;
    }

    /**
     * @param mixed $noticeboard
     */
    public function setNoticeboard($noticeboard)
    {
        $this->noticeboard = $noticeboard;
    }

    public function setLoginAttempts($value)
    {
        $this->loginAttempts = $value;
    }

    public function getUserName()
    {
        return $this->session->get('UserName');
    }

    public function getUserId()
    {
        return $this->session->get('UserId');
    }

    public function __construct(\ModernWays\Dialog\Model\INoticeBoard $noticeBoard,
                                \ModernWays\Identity\Session $session,
                                \ModernWays\Identity\IUser $user)
    {
        $this->noticeboard = $noticeBoard;
        $this->noticeboard->setType('AUTHENTICATION');
        $this->session = $session;
        $this->user = $user;
        $this->loginAttempts = 0;
        $this->isVerified = FALSE;
        $this->noticeboard->startTimeInKey('Authentication Constructor');
        $text = $this->session->isHttps() ? 'secured' : 'insecure';
        if (session_status() == PHP_SESSION_NONE) {
            $this->session->start();
            $this->noticeboard->setText("{$text} {$this->session->getName()} session started in Authentication.");
        } else {
            $this->noticeboard->setText("{$text} {$this->session->getName()} session started outside Authentication.");
        }
        $this->noticeboard->log();
    }

    public function hashPassword($passwordInput)
    {
        return password_hash($passwordInput, PASSWORD_DEFAULT);
    }

    private function verifyPassword($passwordInput, $hashedPasswordOutput)
    {
        $this->noticeboard->startTimeInKey('Authentication verify password');
        if (password_verify($passwordInput, $hashedPasswordOutput)) {
            $this->noticeboard->setText('Password verified');
            $this->noticeboard->log();
            return TRUE;
        }
        $this->noticeboard->setText('Pasword is not verified.');
        $this->noticeboard->log();
        return false;
    }

    private function verifyUser($userNameOutput)
    {
        $this->noticeboard->startTimeInKey('Authentication verify user');
        if ($userNameOutput && $this->getUserName()) {
            if ($userNameOutput == $this->getUserName()) {
                $this->noticeboard->setText('User is verified');
                $this->noticeboard->log();
                return TRUE;
            }
        }
        $this->noticeboard->setText('User is not verified.');
        $this->noticeboard->log();
        return false;
    }

    private function verifyTicket($hashedPasswordOutput)
    {
        $this->noticeboard->startTimeInKey('Authentication verify ticket');
        $ticket = $this->session->get('t');
        $ticketToCheck = $this->session->makeTicket($hashedPasswordOutput);
        if ($ticket == $ticketToCheck) {
            $this->noticeboard->setText('Ticket is verified.');
            $this->noticeboard->log();
            return TRUE;
        }
        $this->noticeboard->setText('Ticket is not verified.');
        $this->noticeboard->log();
        return false;
    }

    private function isBruteForceAttack()
    {
        $this->noticeboard->startTimeInKey('Authentication brute forece attack?');
        if ($this->loginAttempts > 10) {
            $this->noticeboard->setText('Maximum login attemps exceeded.');
            $this->noticeboard->log();
            return true;
        }
        $this->noticeboard->setText('Maximum login attemps not exceeded.');
        $this->noticeboard->log();
        return false;
    }

    public function login($userNameInput, $passwordInput)
    {
        $this->noticeboard->startTimeInKey('Authentication login');
        // de gebruiker wordt opgehaald buiten de login klasse
        // we weten al dat hij/zij bestaat
        if ($this->isBruteForceAttack()) {
            // user is locked out
            return false;
        } else {
            // Does user exists?
            if ($this->user->selectByUserName($userNameInput)) {
                if ($this->verifyPassword($passwordInput, $this->user->getPassword())) {
                    // create Ticket
                    $this->session->setTicket($this->user->getPassword());
                    $this->session->setPositiveInteger('UserId', $this->user->getId());
                    $this->session->setText('UserName', $this->user->getUserName());
                    $this->noticeboard->startTimeInKey('Authentication login');
                    $this->noticeboard->setText('User is logged in.');
                    $this->noticeboard->log();
                    return true;
                } else {
                    $this->noticeboard->startTimeInKey('Authentication login');
                    $this->noticeboard->setText('User is not logged in.');
                    $this->noticeboard->log();
                    return false;
                }
            } else {
                $this->noticeboard->startTimeInKey('Authentication login');
                $this->noticeboard->setText('User does not exist.');
                $this->noticeboard->log();
                return false;
            }

        }
    }

    public function isLoggedIn()
    {
        $isLoggedIn = true;
        if ($this->user->selectByUserName($this->getUserName())) {
            if ($this->verifyTicket($this->user->getPassword())) {
                // logged in
                $isLoggedIn = true;
                $this->noticeboard->startTimeInKey('Authentication is logged in');
                $this->noticeboard->setText('Ticket verified.');
                $this->noticeboard->log();
            } else {
                $this->noticeboard->startTimeInKey('Authentication is not logged in');
                $this->noticeboard->setText('Ticket not verified.');
                $this->noticeboard->log();
            }
        } else {
            $this->noticeboard->startTimeInKey('Authentication is not logged in');
            $this->noticeboard->setText('User does not exist.');
            $this->noticeboard->log();
        }
        return ($isLoggedIn ? $this->getUserId() : $isLoggedIn);
    }

    public function logout()
    {
        $this->session->end();
    }
}



